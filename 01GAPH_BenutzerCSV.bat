@echo off
chcp 1252 > NUL

if !%1!==!! goto :Fehler1


echo Benutzername,Kennwort > C:\Users\%USERNAME%\Desktop\GAPH_Benutzer.csv

setlocal enabledelayedexpansion

for /L %%a in (1,1,%1) do (

echo test_%%a,test_%%a_!random! >> C:\Users\%USERNAME%\Desktop\GAPH_Benutzer.csv

)

endlocal

goto :EOF


:Fehler1
echo.
echo Sie haben nicht alle geforderten Parameter angegeben.
echo.
echo Das Skript %0 muss mit einem Parameter (Anzahl der Datensätze) aufgerufen werden!
echo BSP: %0 17
echo Das Skript wird beendet.
echo.
goto :EOF
