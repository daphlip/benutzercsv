@echo off
REM Quelle für das Ignorieren der letzten Zeile  
REM der Ausgabe von 'net user' ("Der Befehl wurde erfolgreich ausgeführt."):
REM https://stackoverflow.com/questions/11423977/batch-ignore-last-line-of-an-output
REM noch eine schöne FOR Hilfe, die die Parameter gut beschreibt: https://ss64.com/nt/for_f.html

chcp 1252>NUL

IF !%1!==!! goto :Fehler1
IF not exist %1 goto :Fehler2

FOR /F "skip=1 tokens=1,2 delims=," %%B IN (%1) DO (
    
	REM Die Ausgabe von 'net user' nach etwaigen schon vorhandenen Benutzernamen (G/K-Schreibung ingorieren mit /I) durchforsten:
	set /A uservorhanden=0
    FOR /F "skip=4 tokens=1*" %%i IN ('net user ^| findstr /vc:"Der Befehl wurde erfolgreich ausgeführt."') DO (
        IF /I %%B equ %%i (
		    set /A uservorhanden=1
		    set /A logfile=1
		)
    )

    IF %uservorhanden%==0 (
	    net user %%B %%C /add 1>NUL 2>&1
	    rem echo Benutzer %%B wird angelegt.
	) ELSE (
	    echo Benutzer %%B schon vorhanden, wird nicht angelegt >> BenutzerAnlegenError.log
	)

)

if defined logfile (
    echo.
	echo +++++++++++++++++++++++++++++++++++++
    echo Einige Benutzer wurden nicht angelegt!
    echo Siehe Datei BenutzerAnlegenError.log
    echo +++++++++++++++++++++++++++++++++++++
) else (
    echo.
    echo Alle Benutzer wurden erfolgreich angelegt!
)

goto :EOF


:Fehler1
echo.
echo Sie haben nicht alle geforderten Parameter angegeben.
echo.
echo Das Script %0 muss mit einem Prameter (Pfad zur CSV Datei) aufgerufen werden.
echo BSP: %0 C:\Users\%USERNAME%\Desktop\Benutzerliste.csv
echo Das Script wird beendet.
echo.
goto :EOF

:Fehler2
echo.
echo Die Datei mit der Benutzerliste %1 existiert nicht. 
echo Das Script wird beendet.
goto :EOF
